FROM python:3.11.3

WORKDIR /coursework_08

COPY ./requirements.txt /coursework_08/

RUN pip install -r /coursework_08/requirements.txt

COPY . .